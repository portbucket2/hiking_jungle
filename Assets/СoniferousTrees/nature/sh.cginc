#ifndef PORTBLISS_SH_LIGHTING_INCLUDED
#define PORTBLISS_SH_LIGHTING_INCLUDED

	// normal should be normalized, w=1.0
	fixed3 SHEvalLinearL0L1(fixed4 normal)
	{
		fixed3 x;

		// Linear (L1) + constant (L0) polynomial terms
		x.r = dot(unity_SHAr, normal);
		x.g =  dot(unity_SHAg, normal);
		x.b = x.r * x.g;// dot(unity_SHAb, normal);

		return x;
	}

	// normal should be normalized, w=1.0
	fixed3 SHEvalLinearL2(fixed4 normal)
	{
		fixed3 x1, x2;
		// 4 of the quadratic (L2) polynomials
		fixed4 vB = normal.xyzz * normal.yzzx;
		x1.r = dot(unity_SHBr, vB);
		x1.g = dot(unity_SHBg, vB);
		x1.b = dot(unity_SHBb, vB);

		// Final (5th) quadratic (L2) polynomial
		fixed vC = normal.x*normal.x - normal.y*normal.y;
		x2 = unity_SHC.rgb * vC;

		return x1 + x2;
	}

	inline fixed3 LinearToGammaSpace(fixed3 linRGB)
	{
		linRGB = max(linRGB, half3(0.h, 0.h, 0.h));
		// An almost-perfect approximation from http://chilliant.blogspot.com.au/2012/08/srgb-approximations-for-hlsl.html?m=1
		return max(1.055h * pow(linRGB, 0.416666667h) - 0.055h, 0.h);

		// Exact version, useful for debugging.
		//return half3(LinearToGammaSpaceExact(linRGB.r), LinearToGammaSpaceExact(linRGB.g), LinearToGammaSpaceExact(linRGB.b));
	}

	// normal should be normalized, w=1.0
	// output in active color space
	fixed3 ShadeSH9(fixed4 normal)
	{
		// Linear + constant polynomial terms
		fixed3 res = SHEvalLinearL0L1(normal);

		// Quadratic polynomials
		//res += SHEvalLinearL2(normal);

		#ifdef UNITY_COLORSPACE_GAMMA
			res = LinearToGammaSpace(res);
		#endif

		return res;
	}

	fixed3 ShadeSH9Good(fixed4 normal)
	{
		// Linear + constant polynomial terms
		fixed3 res = SHEvalLinearL0L1(normal);

		// Quadratic polynomials
		res += SHEvalLinearL2(normal);

#ifdef UNITY_COLORSPACE_GAMMA
		res = LinearToGammaSpace(res);
#endif

		return res;
	}
#endif