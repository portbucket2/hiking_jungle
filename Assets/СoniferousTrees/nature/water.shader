// Upgrade NOTE: replaced 'defined FOG_COMBINED_WITH_WORLD_POS' with 'defined (FOG_COMBINED_WITH_WORLD_POS)'
// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Portbliss/Water" 
{
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,1)
		_Diffuse("Diffuse", 2D) = "white" {}
		_Shoreline("shore line", 2D) = "white" {}
		_Tile("Tile", float) = 20
		_Additive("Additive vertex pass", Range(0,5)) = 0.2
		_Speed("Speed", float) = 0.05
		_Amount("Amount", float) = 0.05
		_PositionalVarianceX("Positional Variance X", Range(-200,200)) = 0
		_PositionalVarianceY("Positional Variance Y", Range(-200,200)) = 0
		// These are here only to provide default values
		[HideInInspector] _TreeInstanceScale("no idea why it has to be here now", Vector) = (1,1,1,1)
	}

		SubShader
		{
			Tags
			{
				"IgnoreProjector" = "True"
				"Queue" = "Transparent"
			}
			LOD 200
			Pass
			{
				Name "FORWARD"
				Tags { "LightMode" = "ForwardBase" }
				//ColorMask RGB
				ZWrite Off 
				Blend SrcAlpha OneMinusSrcAlpha
				CGPROGRAM
				// compile directives
				#pragma vertex vert_surf
				#pragma fragment frag_surf
				#pragma multi_compile_fog
				#pragma multi_compile_fwdbase
				#include "HLSLSupport.cginc"
				#include "fog.cginc"
				#include "util.cginc"
				
				struct appdata_water
				{
					float4 vertex : POSITION;
					fixed4 texcoord : TEXCOORD0;
				};

				fixed _Additive;
				fixed4 _LightColor0;//updated by unity
				fixed4 _Color;
				sampler2D _Diffuse, _Shoreline;
				half _Tile;
				half _PositionalVarianceX, _PositionalVarianceY, _Amount, _Speed;

				// vertex-to-fragment interpolation data
				struct v2f
				{
					float4 pos : SV_POSITION;
					fixed2 uv : TEXCOORD0;
					fixed3 vlight : TEXCOORD1;
					fixed fogCoord : TEXCOORD2;
				};

				// vertex shader
				v2f vert_surf(appdata_water v)
				{
					v2f o;
					o = (v2f)0;
					o.pos = UnityObjectToClipPos(v.vertex);
					half4 posWp = mul(unity_ObjectToWorld, v.vertex);

					fixed2 tc = v.texcoord;
					fixed2 p = -1.0 + (2.0 * tc) + sin(half2(_PositionalVarianceX, _PositionalVarianceY)) + cos(posWp);
					fixed len = length(p);
					fixed eff = _Speed * _Time[0] * posWp;
					fixed2 uv = tc + (p / len) * cos(len * eff - eff) * _Amount;

					o.uv = uv;
					o.vlight = _Color * _LightColor0.rgb;
					o.vlight += fixed3(_Additive, _Additive, _Additive);
					float z = o.pos.z;
					float properzDist = GET_PROPER_Z_DIST(z);
					o.fogCoord = properzDist * unity_FogParams.z + unity_FogParams.w;
					return o;
				}

				// fragment shader
				fixed4 frag_surf(v2f IN) : SV_Target
				{
					fixed4 col =  tex2D(_Diffuse, IN.uv * _Tile);
					fixed4 shorelineCol = tex2D(_Shoreline, IN.uv);
					col *= fixed4(IN.vlight.rgb, 1) * shorelineCol;
					col.rgb = lerp(unity_FogColor.rgb, col.rgb, saturate(IN.fogCoord.x));
					col.a = shorelineCol.a;
					return col;
				}
			ENDCG
		}
	}
}