// Upgrade NOTE: replaced 'defined FOG_COMBINED_WITH_WORLD_POS' with 'defined (FOG_COMBINED_WITH_WORLD_POS)'
// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Portbliss/Tree" 
{
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,1)
		_ShadowColor("Shadow Color", Color) = (1,1,1,1)
		_Additive("Additive vertex pass", Range(0,5)) = 2
		_Threshold("Threshold toon", Range(0, 1)) = 0.1
		_Cutoff("Alpha cutoff", Range(0,1)) = 0.3
		_MainTex("Base (RGB) Alpha (A)", 2D) = "white" {}
		[Toggle(USE_TOON)] _UseToon("Use Toon Shadow? ", Int) = 0
		// These are here only to provide default values
		[HideInInspector] _TreeInstanceScale("no idea why it has to be here now", Vector) = (1,1,1,1)
	}

		SubShader
		{
			Tags
			{
				"IgnoreProjector" = "True"
			}
			LOD 200
			Pass
			{
				Name "FORWARD"
				Tags { "LightMode" = "ForwardBase" }
				ColorMask RGB

				CGPROGRAM
				// compile directives
				#pragma vertex vert_surf
				#pragma fragment frag_surf
				#pragma multi_compile_fog
				#pragma multi_compile_fwdbase
				#pragma shader_feature USE_TOON
				#include "HLSLSupport.cginc"
				#include "fog.cginc"
				#include "util.cginc"
				#include "sh.cginc"
				#include "terrain.cginc"
				
				struct appdata_tree
				{
					float4 vertex : POSITION;
					fixed3 normal : NORMAL;
					fixed4 texcoord : TEXCOORD0;
				};

				fixed _Additive;
				sampler2D _MainTex;
				fixed4 _ShadowColor;
				fixed _Cutoff;
				fixed4 _LightColor0;//updated by unity
				fixed4 _Color;

				#if defined(USE_TOON)
					fixed _Threshold;
				#endif

				// vertex-to-fragment interpolation data
				struct v2f
				{
					float4 pos : SV_POSITION;
					fixed2 uv : TEXCOORD0;
					fixed3 vlight : TEXCOORD1;
					fixed fogCoord : TEXCOORD2;
					#if defined(USE_TOON)
						fixed nl : TEXCOORD3;
					#endif
				};
			
				// vertex shader
				v2f vert_surf(appdata_tree v)
				{
					v2f o;
					o = (v2f)0;
					o.pos = UnityObjectToClipPos(v.vertex);
					o.uv = v.texcoord;
					fixed3 normalWorld = UnityObjectToWorldNormal(v.normal);
					half nl = dot(normalWorld, _WorldSpaceLightPos0.xyz);
					half clipped_nl = max(0, nl *0.6 + 0.4);
					#if defined(USE_TOON)
						o.nl = nl;
					#endif
					fixed3 shlight = ShadeSH9(fixed4(normalWorld, 1.0));
					o.vlight = shlight * _Color * clipped_nl * _LightColor0.rgb;
					o.vlight += fixed3(_Additive, _Additive, _Additive);
					#if defined(USE_TOON)
					
						if (max(0.0, nl) >= _Threshold)
						{
							o.vlight = _ShadowColor;// = _LightColor0 * _Color * colMain;
						}
					#endif
					float z = o.pos.z;
					o.fogCoord.x = FOG_FACTOR(GET_PROPER_Z_DIST(z));
					return o;
				}

				// fragment shader
				fixed4 frag_surf(v2f IN) : SV_Target
				{
					fixed4 colMain = tex2D(_MainTex, IN.uv);
					fixed4 col = fixed4(colMain.rgb * IN.vlight.rgb, 1);
					clip(colMain.a - _Cutoff);

					//#if defined(USE_TOON)
					//	if (max(0.0, IN.nl) >= _Threshold)
					//	{
					//		col = _ShadowColor * fixed4(IN.vlight, 1);// _LightColor0* _Color* colMain;
					//	}
					//#endif

					col.rgb = lerp(unity_FogColor.rgb, col.rgb, saturate(IN.fogCoord.x));
					return col;
				}
			ENDCG
		}

			// Pass to render object as a shadow caster
			Pass
			{
				Name "ShadowCaster"
				Tags { "LightMode" = "ShadowCaster" }

				CGPROGRAM
				#pragma vertex vert_surf
				#pragma fragment frag_surf
				#pragma multi_compile_shadowcaster
				#include "HLSLSupport.cginc"
				#include "util.cginc"
				#include "shadow.cginc"

				sampler2D _MainTex;
				fixed _Cutoff;

				struct appdata_tree
				{
					float4 vertex : POSITION;
					fixed3 normal : NORMAL;
					fixed2 texcoord : TEXCOORD0;
				};

				struct v2f_surf
				{
					float4 pos : SV_POSITION;
					fixed2 hip_pack0 : TEXCOORD1;
				};

				v2f_surf vert_surf(appdata_tree v)
				{
					v2f_surf o;
					o.hip_pack0.xy = v.texcoord;
					o.pos = UnityClipSpaceShadowCasterPos(v.vertex, v.normal);
					o.pos = UnityApplyLinearShadowBias(o.pos);
					return o;
				}

				fixed4 frag_surf(v2f_surf IN) : SV_Target
				{
					half alpha = tex2D(_MainTex, IN.hip_pack0.xy).a;
					clip(alpha - _Cutoff);
					return 0;
				}
				ENDCG
			}
	}
}