Shader "Portbliss/SplattedTerrain"
{
    Properties
    {
		_Color("Color", color) = (1, 1, 1, 1)
		_ColorAdditive("Road lumen", color) = (0, 0, 0, 0)
		_Overall("Overall lumen", color) = (0, 0, 0, 0)
		_ShadowColor("Shadow Color", color) = (1,1,1,1)
		_SplatMap("Control Map (RGB)", 2D) = "black" {}
		
		_Diff1Map("Layer 1 diffuse (R)", 2D) = "white" {}
		_Tile1("Layer 1 tile", float) = 20
		_Bump1Map("Layer 1 normal (R)", 2D) = "bump" {}

		_Diff2Map("Layer 2 diffuse (G)", 2D) = "white" {}
		_Tile2("Layer 2 tile", float) = 20
		_Bump2Map("Layer 2 normal (G)", 2D) = "bump" {}

		_Diff3Map("Layer 3 diffuse (B)", 2D) = "white" {}
		_Tile3("Layer 3 tile", float) = 20
		_Bump3Map("Layer 3 normal (B)", 2D) = "bump" {}
		[Toggle(USE_PIXEL_LIGHTING)] _UsePixelLighting("Use Pixel Lighting? ", Int) = 1
		[Toggle(USE_VERT_LIGHTING)] _UseVertexLighting("Use Vertex Lighting? ", Int) = 1
		[Toggle(USE_NORMAL_MAPPING)] _UseNormalMapping("Use Normal Mapping? ", Int) = 1
		[Toggle(PC_NORMAL)] _UseBetterNormal("Use PC Normal? ", Int) = 1
		[Toggle(USE_SHADOW)] _UseShadow("Use Shadow? ", Int) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma shader_feature USE_PIXEL_LIGHTING
			#pragma shader_feature USE_VERT_LIGHTING
			#pragma shader_feature USE_NORMAL_MAPPING
			#pragma shader_feature PC_NORMAL
			#pragma shader_feature USE_SHADOW

			#include "util.cginc"
			#include "fog.cginc"

			sampler2D _SplatMap, _Diff1Map, _Diff2Map, _Diff3Map;
			fixed4 _Color, _ColorAdditive, _Overall, _ShadowColor;
			fixed4 _LightColor0;//updated by unity
			float _Tile1, _Tile2, _Tile3;
			#if defined(USE_NORMAL_MAPPING) && defined(USE_PIXEL_LIGHTING) && !defined(USE_VERT_LIGHTING)
				sampler2D _Bump1Map, _Bump2Map, _Bump3Map;
			#endif
			
            struct appdata
            {
                float4 vertex : POSITION;
                half2 uv : TEXCOORD0;
				#if defined(USE_PIXEL_LIGHTING) || defined(USE_VERT_LIGHTING)
					half3 normal : NORMAL;
				#endif
				#if defined(USE_NORMAL_MAPPING) && defined(USE_PIXEL_LIGHTING) && !defined(USE_VERT_LIGHTING)
					half4 tangent : TANGENT;
				#endif
            };

            struct v2f
            {
				float4 vertex : SV_POSITION;
                half2 uv : TEXCOORD0;
				half fogCoord : TEXCOORD1;
				half4 vlight : TEXCOORD2;
				#if defined(USE_PIXEL_LIGHTING)
					half3 normalWorld : TEXCOORD3;
					#if defined(USE_NORMAL_MAPPING)
						half3 tangentWorld : TEXCOORD4;
						half3 binormalWorld : TEXCOORD5;
					#endif
				#endif
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.vlight = fixed4(1,1,1,1);
				#if defined(USE_PIXEL_LIGHTING) || defined(USE_VERT_LIGHTING)
					half3 normalWorld = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);
					
					#if !defined(USE_PIXEL_LIGHTING) && defined(USE_VERT_LIGHTING)
						half nl = dot(normalWorld, _WorldSpaceLightPos0.xyz);
						nl = max(0, nl * 0.6 + 0.4);
						o.vlight.rgb = nl;
					#endif

					#if defined(USE_PIXEL_LIGHTING) && !defined(USE_VERT_LIGHTING)
						o.normalWorld = normalWorld;
					#endif
					
					#if defined(USE_PIXEL_LIGHTING) && defined(USE_NORMAL_MAPPING) && !defined(USE_VERT_LIGHTING)
						o.tangentWorld = normalize(mul(unity_ObjectToWorld, float4(v.tangent.xyz, 0.0)).xyz);
						o.binormalWorld = normalize(cross(o.normalWorld, o.tangentWorld) * v.tangent.w); // tangent.w is specific to Unity
					#endif
				#endif

				float z = o.vertex.z;
				float properzDist = GET_PROPER_Z_DIST(z);
				o.fogCoord = properzDist * unity_FogParams.z + unity_FogParams.w;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 splat = tex2D(_SplatMap, i.uv);
				fixed4 col = fixed4(0, 0, 0, 0);
				fixed4 green = splat.r * tex2D(_Diff1Map, i.uv * _Tile1);
				col += green;
				
				fixed4 road = splat.g * tex2D(_Diff2Map, i.uv * _Tile2);
				col += road;
				
				fixed4 stone = splat.b * tex2D(_Diff3Map, i.uv * _Tile3);
				col += stone;

				col = i.vlight * col * _LightColor0 * _Color + _ColorAdditive * splat.r + _Overall;
				
				#if defined(USE_PIXEL_LIGHTING) && !defined(USE_VERT_LIGHTING)
					#if defined(USE_NORMAL_MAPPING)
						half4 encNorm = half4(0, 0, 0, 0);
						encNorm += splat.r * tex2D(_Bump1Map, i.uv * _Tile1);
						encNorm += splat.g * tex2D(_Bump2Map, i.uv * _Tile2);
						encNorm += splat.b * tex2D(_Bump3Map, i.uv * _Tile3);
						#if defined(PC_NORMAL)
							half3 localCoords = half3(2.0 * encNorm.a - 1.0, 2.0 * encNorm.g - 1.0, 0.0);
							localCoords.z = 1.0 - 0.5 * dot(localCoords, localCoords);
						#else
							half3 localCoords = 2.0 * encNorm.rgb - half3(1.0, 1.0, 1.0);
						#endif

						half3x3 local2WorldTranspose = half3x3(i.tangentWorld, i.binormalWorld, i.normalWorld);

						half3 normalDirection = normalize(mul(localCoords, local2WorldTranspose));
					#else
						half3 normalDirection = i.normalWorld;
					#endif
					
					half nl = dot(normalDirection, _WorldSpaceLightPos0.xyz);
					nl = max(0, nl * 0.6 + 0.4);
					col *= nl;
				#endif
				
				#if defined(USE_SHADOW)
					col = col * splat.a + (1 - splat.a) * _ShadowColor;
				#endif
					
				col.rgb = lerp(unity_FogColor.rgb, col.rgb, saturate(i.fogCoord.x));
                return col;
            }
            ENDCG
        }
    }
}
