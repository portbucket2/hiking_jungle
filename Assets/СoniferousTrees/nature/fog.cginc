#if defined(UNITY_REVERSED_Z)
	#if UNITY_REVERSED_Z == 1
		//D3d with reversed Z => z clip range is [near, 0] -> remapping to [0, far]
		//max is required to protect ourselves from near plane not being correct/meaningfull in case of oblique matrices.
		#define GET_PROPER_Z_DIST(coord) max(((1.0-(coord)/_ProjectionParams.y)*_ProjectionParams.z),0)
	#else
		//GL with reversed z => z clip range is [near, -far] -> should remap in theory but dont do it in practice to save some perf (range is close enough)
		#define GET_PROPER_Z_DIST(coord) max(-(coord), 0)
	#endif
#elif UNITY_UV_STARTS_AT_TOP
	//D3d without reversed z => z clip range is [0, far] -> nothing to do
	#define GET_PROPER_Z_DIST(coord) (coord)
#else
	//Opengl => z clip range is [-near, far] -> should remap in theory but dont do it in practice to save some perf (range is close enough)
	#define GET_PROPER_Z_DIST(coord) (coord)
#endif

#if defined(FOG_LINEAR)
	// factor = (end-z)/(end-start) = z * (-1/(end-start)) + (end/(end-start))
	#define FOG_FACTOR(coord) (coord) * unity_FogParams.z + unity_FogParams.w
#elif defined(FOG_EXP)
	// factor = exp(-density*z)
	#define FOG_FACTOR(coord) exp2(- (unity_FogParams.y * (coord)))
#elif defined(FOG_EXP2)
	// factor = exp(-(density*z)^2)
	#define FOG_FACTOR(coord) exp2(-(unity_FogParams.x * (coord)) * (unity_FogParams.x * (coord)))
#else
	#define FOG_FACTOR(coord) 0.0
#endif