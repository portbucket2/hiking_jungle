#ifndef PORTBLISS_BILLBOARD_INCLUDED
#define PORTBLISS_BILLBOARD_INCLUDED

inline void MakeBillboard(inout float4 vertex, fixed3 normal)
{
	float3 norb = normalize(mul(fixed4(normal, 0), UNITY_MATRIX_IT_MV)).xyz;
	vertex += mul(float4(normal.xy, 0, 0), UNITY_MATRIX_IT_MV);
}

inline void MakeBillboardShadow(inout float4 vertex, fixed3 normal)
{
	float3 norb = normalize(mul(fixed4(normal, 0), UNITY_MATRIX_IT_MV)).xyz;
	vertex += mul(float4(normal.xy, 0, 0), UNITY_MATRIX_IT_MV);
}
#endif