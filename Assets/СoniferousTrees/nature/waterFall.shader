// Upgrade NOTE: replaced 'defined FOG_COMBINED_WITH_WORLD_POS' with 'defined (FOG_COMBINED_WITH_WORLD_POS)'
// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Portbliss/Waterfall" 
{
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,1)
		_NoiseTex("Noise texture", 2D) = "white" {}
		_DisplGuide("Displacement guide", 2D) = "white" {}
		_DisplAmount("Displacement amount", float) = 0
		_TransparencyMul ("Transparency amount", float) = 0
		_Additive("Additive vertex pass", Range(0,3)) = 0
		[HDR]_ColorBottomDark("Dark color bottom", color) = (1,1,1,1)
		[HDR]_ColorTopDark("Dark color top", color) = (1,1,1,1)
		[HDR]_ColorBottomLight("Light color bottom", color) = (1,1,1,1)
		[HDR]_ColorTopLight("Light color top", color) = (1,1,1,1)
		// These are here only to provide default values
		[HideInInspector] _TreeInstanceScale("no idea why it has to be here now", Vector) = (1,1,1,1)
	}

		SubShader
		{
			Tags
			{
				"IgnoreProjector" = "True"
				"Queue" = "Transparent"
			}
			LOD 200
			Pass
			{
				Name "FORWARD"
				Tags { "LightMode" = "ForwardBase" }
				ZWrite Off
				Blend SrcAlpha OneMinusSrcAlpha
				CGPROGRAM
				// compile directives
				#pragma vertex vert_surf
				#pragma fragment frag_surf
				#pragma multi_compile_fog
				#pragma multi_compile_fwdbase
				#include "HLSLSupport.cginc"
				#include "fog.cginc"
				#include "util.cginc"
				
				struct appdata_water
				{
					float4 vertex : POSITION;
					fixed3 normal : NORMAL;
					fixed4 texcoord : TEXCOORD0;
				};

				fixed _Additive;
				fixed4 _LightColor0;//updated by unity
				fixed4 _Color;
				
				sampler2D _NoiseTex;
				sampler2D _DisplGuide;
				half4 _NoiseTex_ST;
				sampler2D _WaterBase;
				fixed4 _ColorBottomDark;
				fixed4 _ColorTopDark;
				fixed4 _ColorBottomLight;
				fixed4 _ColorTopLight;
				half _DisplAmount, _TransparencyMul;

				// vertex-to-fragment interpolation data
				struct v2f
				{
					float4 pos : SV_POSITION;
					fixed2 uv : TEXCOORD0;
					fixed fogCoord : TEXCOORD1;
					fixed4 c1 : TEXCOORD2;
					fixed4 c2 : TEXCOORD3;
				};

				// vertex shader
				v2f vert_surf(appdata_water v)
				{
					v2f o;
					o = (v2f)0;
					o.pos = UnityObjectToClipPos(v.vertex);
					o.uv = v.texcoord * _NoiseTex_ST.xy + _NoiseTex_ST.zw;
					fixed4 vlight = _Color;// *_LightColor0;
					vlight += fixed4(_Additive, _Additive, _Additive, 0);
					float z = o.pos.z;
					float properzDist = GET_PROPER_Z_DIST(z);
					o.fogCoord = properzDist * unity_FogParams.z + unity_FogParams.w;

					o.c1 = lerp(_ColorBottomDark, _ColorTopDark, v.texcoord.y) * vlight;
					o.c2 = lerp(_ColorBottomLight, _ColorTopLight, v.texcoord.y) * vlight;

					return o;
				}

				// fragment shader
				fixed4 frag_surf(v2f IN) : SV_Target
				{
					//Displacement
					half2 displ = tex2D(_DisplGuide, IN.uv + _Time.y / 5).xy;
					displ = ((displ * 2) - 1) * _DisplAmount;

					//Noise
					half3 noiseCol = tex2D(_NoiseTex, float2(IN.uv.x, IN.uv.y + _Time.y / 5) + displ).x;
					half noise = noiseCol.x;
					noise = round(noise * 5.0) / 5.0;
					fixed4 col = lerp(IN.c1, IN.c2, noise);
					
					fixed gray = (noiseCol.r + noiseCol.g + noiseCol.b) / 3;
					col.a = gray * _TransparencyMul;
					col.rgb = lerp(unity_FogColor.rgb, col.rgb, saturate(IN.fogCoord.x));
					
					return col;
				}
			ENDCG
		}
	}
}