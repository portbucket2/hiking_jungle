﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BushPrefabMaker))]
public class BushScriptEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        BushPrefabMaker bushPref = (BushPrefabMaker)target;
        if (GUILayout.Button("Generate"))
        {
            bushPref.EditorStart();
        }
    }

}
