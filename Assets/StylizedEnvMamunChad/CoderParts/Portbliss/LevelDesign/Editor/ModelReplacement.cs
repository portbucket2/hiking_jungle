﻿using UnityEngine;
using UnityEditor;

namespace Portbliss.LevelDesign
{
    public class MyWindow : EditorWindow
    {
        //string myString = "Hello World";
        //bool groupEnabled;
        //bool myBool = true;
        //float myFloat = 1.23f;

        // Add menu named "My Window" to the Window menu
        [MenuItem("Window/My Window")]
        static void Init()
        {
            // Get existing open window or if none, make a new one:
            MyWindow window = (MyWindow)EditorWindow.GetWindow(typeof(MyWindow));
            window.Show();
        }

        //void OnGUI()
        //{
        //    GUILayout.Label("Base Settings", EditorStyles.boldLabel);
        //    myString = EditorGUILayout.TextField("Text Field", myString);
        //    groupEnabled = EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
        //    myBool = EditorGUILayout.Toggle("Toggle", myBool);
        //    myFloat = EditorGUILayout.Slider("Slider", myFloat, -3, 3);
        //    EditorGUILayout.EndToggleGroup();
        //}

        [MenuItem("Window/Automate Tree _m")]
        static void DoIt()
        {
            var sel = Selection.gameObjects;
            var replace = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Portbliss/LevelDesign/Gen/Spruce_5.prefab");
            if (sel == null || sel.Length < 1) 
            { 
                Debug.Log("invalid selection");
            }

            if (replace == null)
            {
                Debug.Log("replace is null!");
            }
            PrintAll(sel);

            if (sel == null || sel.Length < 1 || replace == null) { return; }
            var thisRoot = new GameObject("_NewTreeGroup_" + System.DateTime.Now);
            thisRoot.transform.position = Vector3.zero;
            thisRoot.transform.rotation = Quaternion.identity;
            thisRoot.transform.localScale = Vector3.one;

            foreach (var s in sel)
            {
                var rp = PrefabUtility.InstantiatePrefab(replace) as GameObject;
                rp.transform.SetParent(thisRoot.transform);
                rp.transform.position = s.transform.position;
                rp.transform.rotation = replace.transform.rotation;
                rp.transform.localScale = replace.transform.localScale;
                s.SetActive(false);
            }

        }

        static void PrintAll(GameObject[] allObjs)
        {
            foreach (var g in allObjs)
            {
                Debug.Log("object name :" + g.name);
            }
        }
    }
}