﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapAtScreen : MonoBehaviour
{
    public Camera renderCam;
    public static MapAtScreen instance;
    //public GameObject mapQuad;
    bool screenmap;
    public Animator HandAnim;
    public GameObject HandAnimHolder;
    public Vector3 targetPosHandHolder;
    Vector3 HandHolderPos;
    public Transform UpDowncamera;
    public GameObject camRotHolder;
    float handRotCorr;
    float RotCor;
    public bool MapActivated;
    public Vector3 disabledPos;
    
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        //DisableScreenMap();
    }

    // Update is called once per frame
    void Update()
    {

        if (MapActivated)
        {
            EnableScreenMap();
        }
        else
        {
            HandAnimHolder.transform.localPosition = disabledPos;
        }
        
    }

    public void EnableScreenMap()
    {
        if (pathFollowing.instance.paused && !GameManageMent.instance.GotDeer)
        {
            if (!screenmap)
            {
                HandAnim.SetBool("mapDown", false);
                screenmap = true;
                HandHolderPos = new Vector3(0, 0, 0);
                //handRotCorr = Quaternion.ToEulerAngles(UpDowncamera.localRotation).x * Mathf.Rad2Deg;
                handRotCorr = PlayerMotorInGame.instance.camRotNew.y;
                
            }
            //handRotCorr = Quaternion.ToEulerAngles(UpDowncamera.localRotation).x * Mathf.Rad2Deg;
            //handRotCorr = PlayerMotor.instance.camRotNew.y;
        }
        else
        {
            if (screenmap)
            {
                HandAnim.SetBool("mapDown", true);
                screenmap = false;
                HandHolderPos = targetPosHandHolder;
                handRotCorr = 0;
                
            }
            
        }
        HandAnimHolder.transform.localPosition = Vector3.Lerp(HandAnimHolder.transform.localPosition, HandHolderPos, Time.deltaTime * 3);
        RotCor = Mathf.Lerp(RotCor, handRotCorr, Time.deltaTime * 3);
        camRotHolder.transform.localRotation = Quaternion.Euler(new Vector3(RotCor, 0, 0));

    }

    void DisableScreenMap()
    {
    
        //mapQuad.SetActive(false);
        renderCam.transform.gameObject.SetActive(false);
        screenmap = false;
    }

    public void EnableMap()
    {
        if(MapActivated == false)
        {
            MapActivated = true;
            screenmap = true;
        }
        
    }

    
}
