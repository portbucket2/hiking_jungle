﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsHubManager : MonoBehaviour
{
    public Transform[] itemSpawnPoints;
    public int maxItemsInThisHub;
    public int gotItems;
    public GameObject item;

    public int itemIndexLocal;

    
    public GameObject[] itemsDisplayed;
    public int OwnHubId;
    public GameObject deer;
    //public GameObject deerOn;

    //public bool underGrounded;

    // Start is called before the first frame update
    void Start()
    {
        DisableDeer();
        if (maxItemsInThisHub > ItemHubCentral.instance.itemIndexLatest)
        {
            ItemHubCentral.instance.itemIndexLatest = maxItemsInThisHub-1;
        }
        itemsDisplayed = new GameObject[maxItemsInThisHub];
        ShuffleSpawnItems();
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.U))
        {
            //UpadateItemDispaly();
        }
    }
    
    void ShuffleSpawnItems()
    {
        
        for (int i = 0; i < itemSpawnPoints.Length; i++)
        {
            if(gotItems != maxItemsInThisHub)
            {
                if ((maxItemsInThisHub - gotItems) < (itemSpawnPoints.Length - i))
                {
                    int r = Random.Range(0, 2);
                    if (r == 1)
                    {
                        GameObject go =  itemsDisplayed[gotItems] = Instantiate(item, itemSpawnPoints[i].position , itemSpawnPoints[i].rotation);

                        go.transform.SetParent(itemSpawnPoints[i].transform);
                        itemsDisplayed[gotItems].GetComponent<CollectibleBehavior>().Index = itemIndexLocal;
                        itemsDisplayed[gotItems].GetComponent<CollectibleBehavior>().parentHub = this ;
                        gotItems += 1;
                        itemIndexLocal += 1;
                        
                    }
                }
                else
                {
                    GameObject go = itemsDisplayed[gotItems] = Instantiate(item, itemSpawnPoints[i].position, itemSpawnPoints[i].rotation);

                    go.transform.SetParent(itemSpawnPoints[i].transform);

                    itemsDisplayed[gotItems].GetComponent<CollectibleBehavior>().Index = itemIndexLocal;
                    itemsDisplayed[gotItems].GetComponent<CollectibleBehavior>().parentHub = this;
                    gotItems += 1;
                    itemIndexLocal += 1;
                }


            }
            
            
        }
    }


    public void UpadateItemDispaly(bool reAppear)
    {
        for (int i = 0; i < itemsDisplayed.Length; i++)
        {
            for (int j = 0; j < ItemHubCentral.instance.CollectedItemList.Count; j++)
            {
                if(itemsDisplayed[i].GetComponent<CollectibleBehavior>().Index == ItemHubCentral.instance.CollectedItemList[j])
                {
                    if(ItemHubCentral.instance.itemIndexLatest >= ItemHubCentral.instance.maxItemIndex)
                    {
                        if(ItemHubCentral.instance.AvailableItemList.Count > 0)
                        {
                            itemsDisplayed[i].GetComponent<CollectibleBehavior>().Index = ItemHubCentral.instance.AvailableItemList[0];
                        }
                        
                    }
                    else
                    {
                        itemsDisplayed[i].GetComponent<CollectibleBehavior>().Index = ItemHubCentral.instance.itemIndexLatest;
                    }
                    
                    
                    
                }
            }
            if (reAppear )
            {
                itemsDisplayed[i].GetComponent<CollectibleBehavior>().EnableOwn();
            }
            //if (reAppear && ItemHubCentral.instance.itemIndexLatest <= ItemHubCentral.instance.maxItemIndex)
            //{
            //    itemsDisplayed[i].GetComponent<CollectibleBehavior>().EnableOwn();
            //}
        }
    }

    public void SinkItemHubPos()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, -10, transform.localPosition.z);
    }

    public void UpliftItemHubPos()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z);
    }

    public void EnableDeer()
    {
        deer.SetActive(true);
        GameManageMent.instance.DeerMeshAppear(deer.transform);
        //Debug.Log("Deer");
        //GameManageMent.instance.GetDeer(deer.transform);

        //GameManageMent.instance.GotDeer = true;
    }

    public void DisableDeer()
    {
        deer.SetActive(false);
    }
}
