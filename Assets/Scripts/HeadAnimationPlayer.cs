﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadAnimationPlayer : MonoBehaviour
{
    Animator HeadAnim;
    public AudioSource footstepAudioSource;
    public AudioClip[] footstepsSound;
    public AudioClip CamShutterSound;
    // Start is called before the first frame update
    void Start()
    {
        HeadAnim = GetComponent<Animator>();
        //footstepAudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0) && !Input.GetMouseButton(1) && !Input.GetMouseButton(2) && !Input.GetMouseButton(3))
        {
            if(!pathFollowing.instance.AtDecisionPoint && !pathFollowing.instance.paused)
            {
                HeadAnim.SetBool("HeadWalk", true);
            }
            else
            {
                HeadAnim.SetBool("HeadWalk", false);
            }

        }
        else
        {
            HeadAnim.SetBool("HeadWalk", false);
        }
    }

    public void PlayFootStepSound()
    {
        int i = Random.Range(0, 3);
        //footstepAudioSource.PlayOneShot(footstepsSound[i]);
        footstepAudioSource.clip = footstepsSound[i];

        footstepAudioSource.Play();
    }

    public void CameraShutterSound()
    {
        
        
        footstepAudioSource.clip = CamShutterSound;

        footstepAudioSource.Play();
    }
}
