﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectibleBehavior : MonoBehaviour
{
    public GameObject FillCanvas;
    public Image radialBar;
    public float fillAmount;
    public bool fill;
    public bool highlight;
    public int Index;
    public Text textIndex;

    public GameObject visual;

    public ItemsHubManager parentHub;

    public GameObject[] clues;
    public GameObject[] cluesOutlined;

    public GameObject cluesOutlineHolder;

    int oldItemIndex;
    public GameObject mapClueIndicator;
    public ScriptableItemObj ScriptableItem;
    public GameObject cluesHolder;
    // Start is called before the first frame update
    private void Awake()
    {
        oldItemIndex = 0;
        InstantiatingItems();
        ClueSelectionVisual(Index);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FillCanvas.transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position, Vector3.up);
        radialBar.fillAmount = fillAmount;
        if (fill)
        {
            FillIt();
        }
        else
        {
            fillAmount = 0;
        }
        if (highlight)
        {
            cluesOutlineHolder.SetActive(true);
        }
        else
        {
            cluesOutlineHolder.SetActive(false);
        }
        fill = false;
        highlight = false;

        textIndex.text = "" + Index;

        if(oldItemIndex != Index)
        {
            ClueSelectionVisual(Index);

            //Debug.Log("Enabled " + Index);
            oldItemIndex = Index;
        }
        
    }

    void FillIt()
    {
        if(fillAmount<= 1)
        {
            fillAmount += Time.deltaTime * 1.2f;
        }
        else
        {
            DestroyOwn();
        }
    }

    void DestroyOwn()
    {
        GameObject GC = Instantiate(mapClueIndicator, transform.position, Quaternion.identity);
        GC.transform.SetParent(MapItself.instance.mapItemsHolder.gameObject.transform);
        MapItself.instance.AddItemToMap(GC);
        GC.GetComponent<MapClueIndicator>().UpdateMapClueProperties(Index);
        InventorySystem.instance.InventoryUpdateLatest(Index);


        if(Index == 0)
        {
            MapAtScreen.instance.EnableMap();
        }

        GameManageMent.instance.itemsCollected += 1;
        UiManage.instance.Collected = true;
        UiManage.instance.CollecetdTextVisual(Index);
        CluesFloatingOnUiSystem.instance.AppearClueOnUI(Index);
        DisableOwn();
        //this.transform.gameObject.SetActive(false);
        if ( ItemHubCentral.instance.itemIndexLatest < ItemHubCentral.instance.maxItemIndex)
        {
            ItemHubCentral.instance.IncreaseLatestIndex();
            //ItemHubCentral.instance.AvailableItemList.Add(ItemHubCentral.instance.itemIndexLatest);
        }
            
        ItemHubCentral.instance.UpdateCollectedItems(Index);
        ItemHubCentral.instance. UpdateAllItemDisplay(parentHub);

        UiManage.instance.UpadteUiTexts();

        


    }

    public void EnableOwn()
    {
        visual.SetActive(true);
        
    }
    public void DisableOwn()
    {
        visual.SetActive(false);
    }


    public void ClueSelectionVisual(int Clueindex)
    {
        for (int i = 0; i < clues.Length; i++)
        {
            if(i == Clueindex )
            {
                clues[i].SetActive(true);
                cluesOutlined[i].SetActive(true);
            }
            else
            {
                clues[i].SetActive(false);
                cluesOutlined[i].SetActive(false);
            }
        }
    }

    public void InstantiatingItems()
    {
        clues = new GameObject[ScriptableItem.cluesLvlOne.Length];
        cluesOutlined  = new GameObject[ScriptableItem.cluesLvlOne.Length];
        for (int i = 0; i < clues.Length; i++)
        {
            GameObject go = Instantiate( ScriptableItem.cluesLvlOne[i] , cluesHolder.transform.position, cluesHolder.transform.rotation);
            clues[i] = go;
            go.transform.SetParent(cluesHolder.transform);

            GameObject goo = Instantiate(ScriptableItem.cluesLvlOneOutline [i], cluesOutlineHolder.transform.position, cluesOutlineHolder.transform.rotation);
            cluesOutlined[i] = goo;
            goo.transform.SetParent(cluesOutlineHolder.transform);

        }
    }


}
