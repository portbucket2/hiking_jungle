﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pathFollowing : MonoBehaviour
{
    
    public GameObject target;
    public GameObject PreviousPoint;
    public float walkSpeed;
    public float walkSpeedApp;
    float AdvanceSpeed;
    float RotSpeed;
    //public float zFac;
    public int frames;
    public float initialYAngle;
    public float targetPathLength;
    public float tarYRot;

    public float cosA;

    public float finalYRot;
    float journey;
    public int stepsNeeded;
    Quaternion targetRot;

    public float steer;

    public Vector3 steerVector;
    public float directDistance;

    bool GotInitials;
    public bool paused;
    public bool LevelEnd;
    public bool AtDecisionPoint;
    public static pathFollowing instance;

    public bool easyApproach;
    public float rotSpeedMulti;

    public float Angle0;
    public float Angle1;
    Quaternion idleRot;
    public bool intoDecisionTurn;
    public Transform RightPath;
    public Transform LeftPath;
    
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        if (!easyApproach)
        {
            CalculateInitials();
        }
        rotSpeedMulti = 0;

        idleRot = this.transform.rotation;


    }

    // Update is called once per frame
    void Update()
    {
        //CalculateInitials();
        if (!paused && !AtDecisionPoint)
        {
            MoveAheadNew();
        }

        
        //CalculateInitials();
    }


    void MoveAhead()
    {
        if (target)
        {
            this.transform.Translate(0, 0, AdvanceSpeed * Time.deltaTime * 50f);
            if (rotSpeedMulti < 1)
            {
                rotSpeedMulti += Time.deltaTime*1.5f;
            }
            else
            {
                rotSpeedMulti = 1;
            }

            if (easyApproach)
            {
                steerVector = transform.InverseTransformDirection(-this.transform.position + target.transform.position);

               //if(steerVector.x > 0)
               //{
               //    //steer = Mathf.Lerp(steer, 2 * rotSpeedMulti, Time.deltaTime*2);
               //}
               //else
               //{
               //    //steer = Mathf.Lerp(steer, -2 * rotSpeedMulti, Time.deltaTime * 2);
               //}

                float steerApp = (steerVector.x * RotSpeed) * rotSpeedMulti;
                steerApp = Mathf.Clamp(steerApp, -2f, 2f);
                steer = Mathf.Lerp(steer, steerApp * rotSpeedMulti, Time.deltaTime * 5); 

                
            }

            



            transform.Rotate(0, steer * Time.deltaTime * 50f, 0);
        }
        else
        {
            this.transform.Translate(0, 0, AdvanceSpeed * Time.deltaTime * 50f);
        }
        
    }
    void MoveAheadNew()
    {
        walkSpeedApp = walkSpeed *(1 -(0.5f *(Mathf.Abs(PlayerMotorInGame.instance.diff.x / PlayerMotorInGame.instance.maxSideAngle))));
        if (target)
        {
            idleRot = this.transform.rotation;

            this.transform.position = Vector3.MoveTowards(transform.position, target.transform.position,walkSpeedApp* Time.deltaTime);

            Quaternion lookRot = Quaternion.LookRotation(-transform.position + target.transform.position, transform.up);

            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, lookRot, Time.deltaTime * 2f);
            
        }
        else
        {
            this.transform.rotation = idleRot;

            transform.Translate(0,0, walkSpeedApp * Time.deltaTime);
        }

    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "PathPointNew")
        {
            if (other.GetComponentInParent<pathPointFuntion>().decisionPoint)
            {
                if (!intoDecisionTurn)
                {
                    AtDecisionPoint = true;
                    intoDecisionTurn = true;
                    UiManage.instance.AppearDecisionTurn();
                }
                else
                {
                    intoDecisionTurn = false;

                    target = other.GetComponentInParent<pathPointFuntion>().OutPoint.transform.gameObject;
                }

                RightPath = other.GetComponentInParent<pathPointFuntion>().targetPointBack.transform;

                LeftPath = other.GetComponentInParent<pathPointFuntion>().targetPointFront.transform;


            }

            else
            {
                Vector3 dir0 = (other.transform.position - other.GetComponentInParent<pathPointFuntion>().targetPointFront.transform.position);
                Vector3 dir1 = (other.transform.position - other.GetComponentInParent<pathPointFuntion>().targetPointBack.transform.position);

                Angle0 = Mathf.Rad2Deg * Vector3.AngleBetween(transform.forward, dir0);
                Angle1 = Mathf.Rad2Deg * Vector3.AngleBetween(transform.forward, dir1);

                if (Angle0 < Angle1)
                {
                    target = other.GetComponentInParent<pathPointFuntion>().targetPointBack.transform.gameObject;
                }
                else
                {
                    target = other.GetComponentInParent<pathPointFuntion>().targetPointFront.transform.gameObject;
                }
            }
            other.GetComponentInParent<pathPointFuntion>().VisibleMapPoint();

            //target = other.GetComponentInParent<pathPointFuntion>().targetPointBack.transform.gameObject;
            //idleRot = this.transform.rotation;
        }
        if (other.tag == "SubPoint")
        {
            Vector3 dir0 = (other.transform.position - other.GetComponent<SubPathPoints>().targetPointFront.transform.position);
            Vector3 dir1 = (other.transform.position - other.GetComponent<SubPathPoints>().targetPointBack.transform.position);

            Angle0 = Mathf.Rad2Deg * Vector3.AngleBetween(transform.forward, dir0);
            Angle1 = Mathf.Rad2Deg * Vector3.AngleBetween(transform.forward, dir1);

            if (Angle0 < Angle1)
            {
                target = other.GetComponent<SubPathPoints>().targetPointBack.transform.gameObject;
            }
            else
            {
                target = other.GetComponent<SubPathPoints>().targetPointFront.transform.gameObject;
            }
            //target = other.GetComponent<SubPathPoints>().targetPointBack.transform.gameObject;
            //idleRot = this.transform.rotation;
            other.GetComponent<SubPathPoints>().VisibleMapPoint();
        }

        if(other.tag == "DeerTrigger" && GameManageMent.instance.stars >= 3)
        {
            GameManageMent.instance.GetDeer(other.transform);
            //Debug.Log("Near");
        }
        else if (other.tag == "DeerTriggerFar" && GameManageMent.instance.stars < 3)
        {
            //Debug.Log("Farrr");
            GameManageMent.instance.GetDeer(other.transform);
        }
        //if(other.tag == "pathPoint")
        //{
        //    //PreviousPoint = target;
        //    //target = null;
        //
        //    if (!other.gameObject.GetComponent<pathPointBehavior>().DecisionTurn)
        //    {
        //        //PreviousPoint = target;
        //        target = null;
        //        if (PreviousPoint)
        //        {
        //            if (other.gameObject.GetComponent<pathPointBehavior>().nextPoints[0].gameObject == PreviousPoint)
        //            {
        //                target = other.gameObject.GetComponent<pathPointBehavior>().nextPoints[1].gameObject;
        //            }
        //            else if (other.gameObject.GetComponent<pathPointBehavior>().nextPoints[1].gameObject == PreviousPoint)
        //            {
        //                target = other.gameObject.GetComponent<pathPointBehavior>().nextPoints[0].gameObject;
        //            }
        //            else
        //            {
        //                
        //                Vector3 dir0 = (other.transform.position - other.gameObject.GetComponent<pathPointBehavior>().nextPoints[0].transform.position);
        //                Vector3 dir1 = (other.transform.position - other.gameObject.GetComponent<pathPointBehavior>().nextPoints[1].transform.position);
        //
        //                Angle0 = Mathf.Rad2Deg * Vector3.AngleBetween(transform.forward, dir0);
        //                Angle1 = Mathf.Rad2Deg * Vector3.AngleBetween(transform.forward, dir1);
        //
        //                if (Angle0 < Angle1)
        //                {
        //                    target = other.gameObject.GetComponent<pathPointBehavior>().nextPoints[1].gameObject;
        //                }
        //                else
        //                {
        //                    target = other.gameObject.GetComponent<pathPointBehavior>().nextPoints[0].gameObject;
        //                }
        //
        //            }
        //        }
        //        
        //        else
        //        {
        //            
        //            Vector3 dir0 = ( other.transform.position - other.gameObject.GetComponent<pathPointBehavior>().nextPoints[0].transform.position);
        //            Vector3 dir1 = (other.transform.position - other.gameObject.GetComponent<pathPointBehavior>().nextPoints[1].transform.position);
        //            
        //            Angle0 = Mathf.Rad2Deg* Vector3.AngleBetween(transform.forward, dir0);
        //            Angle1 = Mathf.Rad2Deg * Vector3.AngleBetween(transform.forward , dir1);
        //
        //            if (Angle0 < Angle1)
        //            {
        //                target = other.gameObject.GetComponent<pathPointBehavior>().nextPoints[1].gameObject;
        //            }
        //            else
        //            {
        //                target = other.gameObject.GetComponent<pathPointBehavior>().nextPoints[0].gameObject;
        //            }
        //            
        //        }
        //
        //        if (target.GetComponent<pathPointBehavior>().DecisionTurn && !PreviousPoint.GetComponent<pathPointBehavior>().DecisionTurn)
        //        {
        //            UiManage.instance.DecisionPointScript = target.GetComponent<pathPointBehavior>();
        //            UiManage.instance.AppearDecisionTurn();
        //            AtDecisionPoint = true;
        //
        //            Debug.Log("1111");
        //        }
        //
        //        PreviousPoint = other.transform.gameObject;
        //        //target = null;
        //    }
        //    else
        //    {
        //        if (PreviousPoint.GetComponent<pathPointBehavior>().DecisionTurn)
        //        {
        //            //PreviousPoint = target;
        //            target = null;
        //            target = other.gameObject.GetComponent<pathPointBehavior>().TurnEntryPoint.gameObject;
        //        
        //            Debug.Log("33333");
        //        }
        //        else
        //        {
        //            //PreviousPoint = target;
        //            target = null;
        //            target = other.gameObject.GetComponent<pathPointBehavior>().nextPoints[0].gameObject;
        //
        //            Debug.Log("2222");
        //        }
        //
        //        PreviousPoint = other.transform.gameObject;
        //
        //    }
        //    
        //    if (!easyApproach)
        //    {
        //        CalculateInitials();
        //        
        //    }
        //
        //    rotSpeedMulti = 0;
        //
        //
        //
        //}
        if (other.tag == "LevelEndBox" && !LevelEnd)
        {
            LevelEnd = true;
            //UiManage.instance.ManagePanelAppearances();
        }
        if (other.tag == "hubSensor" )
        {
            if (other.GetComponent<HubAriseSensorManager>().automatic)
            {
                other.GetComponent<HubAriseSensorManager>().DecideHubId(transform);
                //ItemHubCentral.instance.AriseHub(i);
            }
            
        }
        if (other.tag == "checkout")
        {

            ItemHubCentral.instance.AriseHubCheckout();
        }

    }


    void CalculateInitials()
    {
        steerVector = transform.InverseTransformDirection(-this.transform.position + target.transform.position);
        directDistance = steerVector.magnitude;

        //stepsNeeded = (int)(directDistance / AdvanceSpeed);

        //steer = steerVector.x * RotSpeed *(1 - directDistance* zFac* Mathf.Abs( steerVector.x ));
        //steer = (steerVector.x / ((directDistance * zFac) * (directDistance * zFac))) * RotSpeed;

        initialYAngle =Mathf.Rad2Deg * Mathf.Atan2((steerVector.x + 0.001f) , steerVector.z);
        //
        cosA = Mathf.Abs(Mathf.Cos ( Mathf.Deg2Rad * (Mathf.Abs(90 - initialYAngle))));
        float radius = (steerVector.magnitude) / (2 * cosA);
        targetPathLength = radius * Mathf.Deg2Rad *(Mathf.Abs(initialYAngle * 2));
        //
        stepsNeeded =(int)Mathf.Abs(targetPathLength / AdvanceSpeed);
        steer  = (initialYAngle* 2) / stepsNeeded;
        //RotSpeed = initialYAngle / stepsNeeded;
        //steerVector = transform.InverseTransformDirection(-this.transform.position + target.transform.position);
        //directDistance = steerVector.magnitude;


    }

    public void LeaveDecisionPoint()
    {
        AtDecisionPoint = false;
    }

    public void RightDecision()
    {
        target = RightPath.transform.gameObject;
    }
    public void LeftDecision()
    {
        target = LeftPath.transform.gameObject;
    }
}
