﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pathPointBehavior : MonoBehaviour
{
    public Transform[] nextPoints;
    public bool DecisionTurn;
    public Transform RightNextPoint;
    public Transform LeftNextPoint;
    public Transform TurnEntryPoint;
    //public Transform TurnExitPoint;
    //public Button RightButton;
    //public Button LeftButton;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoRight()
    {
        nextPoints[0] = RightNextPoint;
        //TurnEntryPoint = RightNextPoint;
        UiManage.instance.DisAppearDecisionTurn();
        pathFollowing.instance.LeaveDecisionPoint();
    }

    public void GoLeft()
    {
        nextPoints[0] = LeftNextPoint;
        //TurnEntryPoint = LeftNextPoint;
        UiManage.instance.DisAppearDecisionTurn();
        pathFollowing.instance.LeaveDecisionPoint();
    }


}
