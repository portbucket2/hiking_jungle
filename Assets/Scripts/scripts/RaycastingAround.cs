﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastingAround : MonoBehaviour
{
    public LayerMask collectibleLayer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Raycasting();
    }

    void Raycasting()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position , Camera.main.transform.forward, out hit, 30f , collectibleLayer))
        {
            //Debug.Log(hit.transform.gameObject.name);

            hit.transform.gameObject.GetComponentInParent <CollectibleBehavior>().fill = true;
        }

        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 200f, collectibleLayer))
        {
            //Debug.Log(hit.transform.gameObject.name);

            hit.transform.gameObject.GetComponentInParent<CollectibleBehavior>().highlight = true;
        }
    }
}
