﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySystem : MonoBehaviour
{
    public static InventorySystem instance;
    public InventoryItem[] InvItemButtons;
    public Text InVInfo;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;

        
    }
    void Start()
    {
        //for (int i = 0; i < InvItemButtons.Length; i++)
        //{
        //    InvItemButtons[i].InvItmIndex = i;
        //}

        //StartFuntionOfInventorySystem();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InventoryUpdateLatest(int Index )
    {
        InvItemButtons[Index].EnableInvItem(Index);
        //Debug.Log("Inv " + Index);
    }
    public void HighlightRightInvButton(int Index)
    {
        for (int i = 0; i < InvItemButtons.Length; i++)
        {
            if(i == Index)
            {
                InvItemButtons[i].HighlightMe();
            }
            else
            {
                InvItemButtons[i].NonHighlightMe();
            }
            
        }
    }

    public void StartFuntionOfInventorySystem()
    {
        for (int i = 0; i < InvItemButtons.Length; i++)
        {
            InvItemButtons[i].InvItmIndex = i;
            InvItemButtons[i].DisableInvItem();
        }
    }
}
