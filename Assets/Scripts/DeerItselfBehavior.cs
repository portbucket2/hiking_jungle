﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeerItselfBehavior : MonoBehaviour
{
    public Animator deerMeshAnim;
    public GameObject deerHolder;
    public GameObject deerVisual;
    public Transform RunDestination;
    public bool LookAt;
    public bool Run;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RunToDest();

        if (Input.GetKeyDown(KeyCode.H))
        {
            TriggerHeadRise();
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            TriggerRun();
        }
    }

    void RunToDest()
    {
        if (Run)
        {
            deerHolder.transform.position = Vector3.MoveTowards(deerHolder.transform.position, RunDestination.position, Time.deltaTime * 30);
            if(Vector3.Distance( deerHolder.transform.position , RunDestination.position) < 0.2f)
            {
                //Run = false;
                DeerMeshReset();
            }
        }
        
    }

    public void TriggerRun()
    {
        if (LookAt)
        {
            Run = true;
            deerMeshAnim.SetTrigger("deerRun");

        }

        LookAt = false;
    }

    public void TriggerHeadRise()
    {
        LookAt= true;
        deerMeshAnim.SetTrigger("deerHeadRise");
    }

    public void DeerMeshReset()
    {
        deerVisual.SetActive(false);
        deerMeshAnim.SetTrigger("deerIdle");
        Run = false;
        LookAt = false;
        deerVisual.SetActive(false);
        deerHolder.transform.localPosition = new Vector3(0, 0, 0);

    }
    public void AppearDeerMesh(int star)
    {
        DeerMeshReset();
        deerVisual.SetActive(true);

        float Angle = Vector3.SignedAngle(transform.forward, PlayerMotorInGame.instance.transform.position - transform.position, transform.up);

        //Debug.Log("Angle " + Angle);

        if(Angle > 0)
        {
            Quaternion AngleCor = Quaternion.Euler(0, 180, 0);
            Quaternion OwnAngle = transform.rotation;
            transform.rotation = OwnAngle * AngleCor;
        }

        if(star< 3)
        {
            transform.Rotate(0, 70, 0);
        }
        

    }
}
