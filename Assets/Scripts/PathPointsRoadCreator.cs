﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathPointsRoadCreator : MonoBehaviour
{
    // Start is called before the first frame update
    public pathPointBehavior[] pathPointBehaviors;
    public GameObject ArrowIn;
    public GameObject ArrowOut;

    public pathPointBehavior PathIn;
    public pathPointBehavior PathOut;
    // Start is called before the first frame update
    private void Awake()
    {
        for (int i = 0; i < pathPointBehaviors.Length; i++)
        {
            if(i == 0)
            {
                pathPointBehaviors[i].nextPoints[0] = pathPointBehaviors[i + 1].gameObject.transform;
                if(PathIn)
                    pathPointBehaviors[i].nextPoints[1] = PathIn.gameObject.transform;
                ArrowIn.transform.position = pathPointBehaviors[i].gameObject.transform.position;
        
            }
            else if (i > 0 && i < pathPointBehaviors.Length - 1)
            {
                pathPointBehaviors[i].nextPoints[0] = pathPointBehaviors[i + 1].gameObject.transform;
                pathPointBehaviors[i].nextPoints[1] = pathPointBehaviors[i - 1].gameObject.transform;
            }
            else
            {
                if(PathOut)
                    pathPointBehaviors[i].nextPoints[0] = PathOut.gameObject.transform;
                pathPointBehaviors[i].nextPoints[1] = pathPointBehaviors[i - 1].gameObject.transform;
                ArrowOut.transform.position = pathPointBehaviors[i].gameObject.transform.position;
            }
        }
    }
    void Start()
    {
        //for (int i = 0; i < pathPointBehaviors.Length; i++)
        //{
        //    if (i == 0)
        //    {
        //        pathPointBehaviors[i].nextPoints[0] = pathPointBehaviors[i + 1].gameObject.transform;
        //        if (PathIn)
        //            pathPointBehaviors[i].nextPoints[1] = PathIn.gameObject.transform;
        //
        //        ArrowIn.transform.position = pathPointBehaviors[i].gameObject.transform.position;
        //
        //    }
        //    else if (i > 0 && i < pathPointBehaviors.Length - 1)
        //    {
        //        pathPointBehaviors[i].nextPoints[0] = pathPointBehaviors[i + 1].gameObject.transform;
        //        pathPointBehaviors[i].nextPoints[1] = pathPointBehaviors[i - 1].gameObject.transform;
        //    }
        //    else
        //    {
        //        if (PathOut)
        //            pathPointBehaviors[i].nextPoints[0] = PathOut.gameObject.transform;
        //
        //        pathPointBehaviors[i].nextPoints[1] = pathPointBehaviors[i - 1].gameObject.transform;
        //        ArrowOut.transform.position = pathPointBehaviors[i].gameObject.transform.position;
        //    }
        //}

        //ArrowRot();
    }

    // Update is called once per frame
    void Update()
    {
        ArrowRot();
    }

    void ArrowRot()
    {
        ArrowIn.transform.rotation = Quaternion.LookRotation(ArrowIn.transform.position - PathIn.gameObject.transform.position, Vector3.up);

        ArrowOut.transform.rotation = Quaternion.LookRotation(-ArrowOut.transform.position + PathOut.gameObject.transform.position, Vector3.up);
    }
}
