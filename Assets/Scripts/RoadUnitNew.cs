﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadUnitNew : MonoBehaviour
{
    public pathPointFuntion[] pathPointsRoad;
    // Start is called before the first frame update
    void Awake()
    {
        AssignPathPoints();
    }
    void Start()
    {
        //AssignPathPoints();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void AssignPathPoints()
    {
        for (int i = 0; i < pathPointsRoad.Length; i++)
        {
            if(i > 0)
            {
                pathPointsRoad[i].PreviousPathPoint = pathPointsRoad[i - 1];
                pathPointsRoad[i-1].nextPathPoint = pathPointsRoad[i ];
            }
        }
    }
}
