﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecisionTurnRGB : MonoBehaviour
{
    public pathPointBehavior blockRed;
    public pathPointBehavior blockGreen;
    public pathPointBehavior blockBlue;

    public PathPointsRoadCreator RoadRed;
    public PathPointsRoadCreator RoadGreen;
    public PathPointsRoadCreator RoadBlue;

    public bool RedIn;
    public bool GreenIn;
    public bool BlueIn;
    // Start is called before the first frame update
    private void Awake()
    {
        
    }
    void Start()
    {
        CalculateBoolIn();
        GetEntryPoints();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CalculateBoolIn()
    {
        float dis0 = Vector3.Distance(blockRed.transform.position, RoadRed.ArrowIn.transform.position);
        float dis1 = Vector3.Distance(blockRed.transform.position, RoadRed.ArrowOut.transform.position);
        if(dis0 > dis1)
        {
            RedIn = true;
        }
        else
        {
            RedIn = false;
        }

        dis0 = Vector3.Distance(blockGreen.transform.position, RoadGreen.ArrowIn.transform.position);
        dis1 = Vector3.Distance(blockGreen.transform.position, RoadGreen.ArrowOut.transform.position);
        if (dis0 > dis1)
        {
            GreenIn = true;
        }
        else
        {
            GreenIn = false;
        }

        dis0 = Vector3.Distance(blockBlue.transform.position, RoadBlue.ArrowIn.transform.position);
        dis1 = Vector3.Distance(blockBlue.transform.position, RoadBlue.ArrowOut.transform.position);
        if (dis0 > dis1)
        {
            BlueIn = true;
        }
        else
        {
            BlueIn = false;
        }
    }

    void GetEntryPoints()
    {
        if (RedIn)
        {
            blockRed.TurnEntryPoint = RoadRed.pathPointBehaviors[RoadRed.pathPointBehaviors.Length - 1].transform;
            RoadRed.PathOut = blockRed;
            RoadRed.pathPointBehaviors[RoadRed.pathPointBehaviors.Length - 1].nextPoints[0] = RoadRed.PathOut.gameObject.transform;
        }
        else
        {
            blockRed.TurnEntryPoint = RoadRed.pathPointBehaviors[0].transform;
            RoadRed.PathIn = blockRed;
            RoadRed.pathPointBehaviors[0].nextPoints[1] = RoadRed.PathIn.gameObject.transform;
        }

        if (GreenIn)
        {
            blockGreen.TurnEntryPoint = RoadGreen.pathPointBehaviors[RoadGreen.pathPointBehaviors.Length - 1].transform;
            RoadGreen.PathOut = blockGreen;
            RoadGreen.pathPointBehaviors[RoadGreen.pathPointBehaviors.Length - 1].nextPoints[0] = RoadGreen.PathOut.gameObject.transform;
        }
        else
        {
            blockGreen.TurnEntryPoint = RoadGreen.pathPointBehaviors[0].transform;
            RoadGreen.PathIn = blockGreen;
            RoadGreen.pathPointBehaviors[0].nextPoints[1] = RoadGreen.PathIn.gameObject.transform;
        }

        if (BlueIn)
        {
            blockBlue.TurnEntryPoint = RoadBlue.pathPointBehaviors[RoadBlue.pathPointBehaviors.Length - 1].transform;
            RoadBlue.PathOut = blockBlue;
            RoadBlue.pathPointBehaviors[RoadBlue.pathPointBehaviors.Length - 1].nextPoints[0] = RoadBlue.PathOut.gameObject.transform;
        }
        else
        {
            blockBlue.TurnEntryPoint = RoadBlue.pathPointBehaviors[0].transform;
            RoadBlue.PathIn = blockBlue;
            RoadBlue.pathPointBehaviors[0].nextPoints[1] = RoadBlue.PathIn.gameObject.transform;
        }
    }
}
