﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItem : MonoBehaviour
{
    public Sprite[] itemicons;
    public int InvItmIndex;
    public Image ownImage;
    public Image highlightImage;
    public Text OwnText;
    public GameObject QuestionMark;
    public Text InfoTxt;
    // Start is called before the first frame update
    void Start()
    {
        //DisableInvItem();

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EnableInvItem( int index )
    {
        ownImage.sprite = itemicons[index];
        QuestionMark.SetActive(false);
        ownImage.gameObject.SetActive(true);
        OwnText.text = UiManage.instance.clueCollectionTexts[index];
        OwnText.gameObject.SetActive(true);
        GetComponent<Button>().onClick.RemoveAllListeners();
        GetComponent<Button>().onClick.AddListener(InvButtonClickMe);
        //Debug.Log("Inv in  " + index);
    }

    public void DisableInvItem()
    {
        //ownImage.sprite = itemicons[index];
        QuestionMark.SetActive(true);
        ownImage.gameObject.SetActive(false);
        OwnText.gameObject.SetActive(false);
        GetComponent<Button>().onClick.AddListener(InvButtonDisabled);
    }

    public void InvButtonClickMe()
    {
        InfoTxt.text = UiManage.instance.clueCollectionTextsDescription[InvItmIndex];
        InventorySystem.instance.HighlightRightInvButton(InvItmIndex);
    }

    public void InvButtonDisabled()
    {
        InfoTxt.text = "Not Collected yet";
        InventorySystem.instance.HighlightRightInvButton(InvItmIndex);
    }

    public void HighlightMe()
    {
        highlightImage.gameObject.SetActive(true);
    }
    public void NonHighlightMe()
    {
        highlightImage.gameObject.SetActive(false);
    }
}
