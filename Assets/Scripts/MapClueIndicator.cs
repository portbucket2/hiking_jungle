﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapClueIndicator : MonoBehaviour
{
    public int MapClueIndex;
    public Text mapClueName;
    public Image mapClueImage;
    //public Sprite[] clueSprites;
    public ScriptableItemObj ScriptableItem;
    // Start is called before the first frame update
    void Start()
    {
        //UpdateMapClueProperties();
        
    }
    //
    //// Update is called once per frame
    //void Update()
    //{
    //    
    //}

    public void UpdateMapClueProperties(int index)
    {
        MapClueIndex = index;
        mapClueName.text = UiManage.instance.clueCollectionTexts[MapClueIndex];
        
        mapClueImage.sprite = ScriptableItem.MapIconsLvlOne[index];

    }

    
}
