﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pathPointFuntion : MonoBehaviour
{
    public Transform frontHandle;
    public Transform backHandle;

    public pathPointFuntion nextPathPoint;
    public pathPointFuntion PreviousPathPoint;

    public Vector3 frontDrawPoint;
    public Vector3 backDrawPoint;
    public Vector3 connectDrawPoint;
    public float progression;
    public float progressionDone;

    public GameObject indiFr;
    public GameObject indiBack;

    public GameObject indiFinal;

    public GameObject subPoint;
    public float delayThreshold;
    float delay;
    public int Steps;
    public GameObject[] subPointsGO;
    public SubPathPoints[] subPointsGOScripts;
    public GameObject subPointsHolder;

    public Transform targetPointFront;
    public Transform targetPointBack;
    int subPointIndex;

    public bool decisionPoint;
    public pathPointFuntion OutPoint;
    bool outPointUpdated;

    public GameObject mapPoint;
    public GameObject myMapPoint;
    bool mapPointUpdated;
    // Start is called before the first frame update
    public void Awake()
    {
        CreatMapPoint();
    }
    void Start()
    {
        subPointsGO = new GameObject[Steps];
        subPointsGOScripts = new SubPathPoints[Steps];
        delayThreshold = (float)1 / (Steps + 1);
        
        while(progression < 1f)
        {
            
            //DrawPoints();
            
            if(progression >= delayThreshold)
            {
                DrawPoints();
                //Debug.Log(progression);
            }
            progression += delayThreshold;
            

        }

        if (decisionPoint)
        {
            if (!OutPoint.targetPointFront)
            {
                OutPoint.targetPointFront = this.transform;
            }
            else if (!OutPoint.targetPointBack)
            {
                OutPoint.targetPointBack = this.transform;
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (decisionPoint && !outPointUpdated)
        {
            if (!OutPoint.targetPointFront)
            {
                OutPoint.targetPointFront = this.transform;
            }
            else if (!OutPoint.targetPointBack)
            {
                OutPoint.targetPointBack = this.transform;
            }

            outPointUpdated = true;
        }

        if (Input.GetKey(KeyCode.D))
        {
            DrawPoints();
        }
        
        //Instantiate();
    }

    void DrawPoints()
    {
        //if(progression < 1)
        //{
        //    progression += Time.deltaTime;
        //
        //    if ((progression - progressionDone) > delayThreshold)
        //    {
        //        progressionDone = progression;
        //        Instantiate(subPoint, indiFinal.transform.position, Quaternion.identity);
        //    }
        //
        //    CreatSubPoints();
        //
        //}
        //else
        //{
        //    progression = 1;
        //}

        
        if (PreviousPathPoint)
        {
            backDrawPoint = Vector3.Lerp( backHandle.position, transform.position, progression);
        }
        else
        {
            backDrawPoint = transform.position;
        }

        if (nextPathPoint)
        {
            frontDrawPoint = Vector3.Lerp(transform.position, frontHandle.position, progression);
            connectDrawPoint = Vector3.Lerp(frontHandle.position,nextPathPoint.backHandle.position, progression);

        }
        else
        {
            frontDrawPoint = transform.position;
        }

        indiFr.transform.position = frontDrawPoint;
        indiBack.transform.position = backDrawPoint;

        if ((nextPathPoint))
        {
            Vector3 drawOne = Vector3.Lerp(frontDrawPoint, connectDrawPoint, progression);
            Vector3 drawTwo = Vector3.Lerp( connectDrawPoint, nextPathPoint.indiBack.transform.position, progression);

            indiFinal.transform.position = Vector3.Lerp(drawOne, drawTwo, progression);

            subPointsGO[subPointIndex] =  Instantiate(subPoint, indiFinal.transform.position, Quaternion.identity);
            subPointsGO[subPointIndex].GetComponent<SubPathPoints>().CreatMapPoint();

            subPointsGOScripts[subPointIndex] = subPointsGO[subPointIndex].GetComponent<SubPathPoints>();

            subPointsGO[subPointIndex].transform.SetParent(subPointsHolder.transform);

            ManageSubPathPoints(subPointIndex);

            subPointIndex += 1;

        }
        
    }

    void ManageSubPathPoints(int index)
    {
        if(index == 0)
        {
            //subPointsGOScripts[subPointIndex].targetPointFront = subPointsGOScripts[subPointIndex + 1].transform;
            subPointsGOScripts[subPointIndex].targetPointBack = this.transform;

            targetPointFront = subPointsGOScripts[subPointIndex].transform;
        }
        else if(index > 0 && index < Steps - 1)
        {
            //subPointsGOScripts[subPointIndex].targetPointFront = subPointsGOScripts[subPointIndex + 1].transform;
            subPointsGOScripts[subPointIndex].targetPointBack = subPointsGOScripts[subPointIndex - 1].transform;

            subPointsGOScripts[subPointIndex - 1].targetPointFront = subPointsGOScripts[subPointIndex ].transform;

        }
        else if(index == Steps - 1)
        {
            subPointsGOScripts[subPointIndex].targetPointFront = nextPathPoint.transform;
            subPointsGOScripts[subPointIndex].targetPointBack = subPointsGOScripts[subPointIndex - 1].transform;

            subPointsGOScripts[subPointIndex - 1].targetPointFront = subPointsGOScripts[subPointIndex].transform;

            nextPathPoint.targetPointBack = subPointsGOScripts[subPointIndex].transform;
        }
    }

    public void CreatMapPoint()
    {
        myMapPoint = Instantiate(mapPoint, transform.position, transform.rotation);
        myMapPoint.transform.SetParent(MapItself.instance.mapHolder);
        myMapPoint.SetActive(false);
    }

    public void VisibleMapPoint()
    {
        //myMapPoint.SetActive(false);
        if (!mapPointUpdated)
        {
            MapItself.instance.UpdatemapPointAppIndex();
            if (MapItself.instance.mapPointAppIndex == 0)
            {
                myMapPoint.SetActive(true);
            }
            mapPointUpdated = true;
        }

    }

    //void CreatSubPoints()
    //{
    //    delay += Time.deltaTime;
    //    if(delay > delayThreshold)
    //    {
    //        delay = 0;
    //        Instantiate(subPoint, indiFinal.transform.position, Quaternion.identity);
    //    }
    //
    //}
}
