﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotorInGame : MonoBehaviour
{
    public static PlayerMotorInGame instance;
    public float walkSpeed;
    float walkSpeedApp;
    public float maxRotSpeed;
    float rotSpeedApp;
    public Vector2 trackPos;
    public GameObject camHolder;
    public GameObject camHolderSide;

    public Vector2 camRotApp;
    //public float maxcamRot;
    public float clickInY;
    public float clickInX;
    public Vector2 camRotProgress;
    public Vector2 camRotOld;
    public Vector2 camRotNew;

    public float maxSideAngle;

    public float snapSpeed;

    public bool rotDir;

    public Vector3 diff;

    public bool gotDear
    {
        get
        {
            return GameManageMent.instance.GotDeer;
        }
    }
    public Transform deerTarget;
    

    

    
    public float ClampAngle;

    public bool LocalRotMode;

    public GameObject Dummy;
    public GameObject DummyPointer;

    public float camrot;
    public float camrotCorr;

    public bool clicked;

    

    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        if (!LocalRotMode)
        {
            AllignSideCamera();
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        Application.targetFrameRate = 60;
        ManageScreenTapPoint();

        ManageCamHolderRot();
        if (LocalRotMode)
        {
            ManageCamHolderLocalRot();
        }
        

        ManageWalk();
        //ManageDummy(-camRotNew.x, camRotNew.y);
        //RotateCamNew();

        //if (!gotDear)
        //{
        //   
        //    ManageWalk();
        //}
        //else
        //{
        //    pathFollowing.instance.paused = true;
        //}
        //else
        //{
        //    //camHolderSide.transform
        //}



        FollowPathFollower();





    }

    void ManageScreenTapPoint()
    {
        trackPos.x = (Input.mousePosition.x / Screen.width) - 0.5f;
        trackPos.y = (Input.mousePosition.y / Screen.height);

        if (Input.GetMouseButton(0) && !Input.GetMouseButton(1) && !Input.GetMouseButton(2) && !Input.GetMouseButton(3) && !UiManage.instance.AtUi)
        {
            clicked = true;
        }
        else
        {
            clicked = false;
        }
    }

    void ManageWalk()
    {
        if (Input.GetMouseButtonDown(0) && !Input.GetMouseButton(1) && !Input.GetMouseButton(2) && !Input.GetMouseButton(3))
        {
            //camRotNew.x = -diffStored.y;
            camRotOld = camRotNew;
            clickInY = trackPos.y;
            clickInX = trackPos.x;
        }
        if (clicked)
        {
            if (!gotDear)
            {
                pathFollowing.instance.paused = false;
            }
            else
            {
                pathFollowing.instance.paused = true;
            }
            
            walkSpeedApp = Mathf.Lerp(walkSpeedApp, walkSpeed, Time.deltaTime * snapSpeed);
            if(Mathf.Abs(trackPos.x) > 0.2f)
            {
                rotSpeedApp = Mathf.Lerp(rotSpeedApp, trackPos.x * maxRotSpeed, Time.deltaTime * snapSpeed);
            }
            else
            {
                rotSpeedApp = Mathf.Lerp(rotSpeedApp, 0, Time.deltaTime * snapSpeed);
            }

            camRotProgress.y = clickInY - trackPos.y;
            camRotProgress.x = clickInX - trackPos.x;

            camRotNew.y = camRotOld.y + camRotProgress.y * 200f;

            
            camRotNew.x = camRotOld.x + camRotProgress.x * 150f;
            camRotNew.y = Mathf.Clamp(camRotNew.y, -40f, 40f);

            //camRotNew.x = Mathf.Clamp(camRotNew.x, -60f, 60f);
            camRotNew.x = Mathf.Clamp(camRotNew.x, ClampAngle - maxSideAngle, ClampAngle + maxSideAngle);



            camRotApp.y = Mathf.Lerp(camRotApp.y, camRotNew.y, Time.deltaTime *snapSpeed);
            camRotApp.x = Mathf.Lerp(camRotApp.x, camRotNew.x, Time.deltaTime * snapSpeed);
        }
        else
        {
            pathFollowing.instance.paused = true;
            walkSpeedApp = Mathf.Lerp(walkSpeedApp, 0, Time.deltaTime * snapSpeed);
            rotSpeedApp = Mathf.Lerp(rotSpeedApp, 0, Time.deltaTime * snapSpeed);
            camRotProgress.y = 0;
            camRotProgress.x = 0;

            camRotApp.y = Mathf.Lerp(camRotApp.y, camRotNew.y, Time.deltaTime * snapSpeed);
            camRotApp.x = Mathf.Lerp(camRotApp.x, camRotNew.x, Time.deltaTime * snapSpeed);

            camRotNew.y = Mathf.Clamp(camRotNew.y, -40f, 40f);
            //camRotNew.x = Mathf.Clamp(camRotNew.x, -60f, 60f);
            camRotNew.x = Mathf.Clamp(camRotNew.x, ClampAngle - maxSideAngle, ClampAngle + maxSideAngle);


        }

        //transform.Translate(0, 0, walkSpeedApp*(1 - Mathf.Abs(trackPos.x * 2)) * Time.deltaTime * 50f);
        //transform.Translate(0, 0, walkSpeedApp  * Time.deltaTime * 50f);
        if (rotDir)
        {
            transform.Rotate(0, rotSpeedApp * Time.deltaTime * 50f, 0);
        }

        


    }

    void ManageCamHolderRot()
    {
        camHolder.transform.localRotation = Quaternion.Lerp(camHolder.transform.localRotation, Quaternion.Euler(camRotApp.y, 0, 0), Time.deltaTime * 10);

        //camHolderSide.transform.position = this.transform.position;
        Vector3 locRot = Mathf.Rad2Deg * Quaternion.ToEulerAngles(camHolderSide.transform.localRotation);
        //Debug.Log(locRot);

        if (!gotDear)
        {
            Quaternion sideRot = Quaternion.Euler(0, -camRotApp.x, 0);
            camHolderSide.transform.rotation = Quaternion.Lerp(camHolderSide.transform.rotation, sideRot, Time.deltaTime * 10);
        }
        else
        {
            Quaternion sideRot = Quaternion.LookRotation((-camHolderSide.transform.position + deerTarget.position), transform.up);
            camHolderSide.transform.rotation = Quaternion.Lerp(camHolderSide.transform.rotation, sideRot, Time.deltaTime * 2);
        }
        
        

        
        diff.y = Vector3.SignedAngle(transform.forward, camHolder.transform.forward, transform.up);

        diff.x = Vector3.SignedAngle(transform.forward, camHolderSide.transform.forward, transform.up);

        ClampAngle = camRotApp.x + diff.x;

       



        //if (Input.GetMouseButtonDown(0))
        //{
        //    //camRotNew.x = -diffStored.y;
        //    camRotOld = camRotNew;
        //    clickInY = trackPos.y;
        //    clickInX = trackPos.x;
        //}
        //if (Input.GetMouseButtonUp(0))
        //{
        //    
        //}
    }

    void ManageCamHolderLocalRot()
    {
        camHolder.transform.localRotation = Quaternion.Euler(camRotApp.y, 0, 0);

        //camHolderSide.transform.position = this.transform.position;
        Vector3 locRot = Mathf.Rad2Deg * Quaternion.ToEulerAngles(camHolderSide.transform.localRotation);
        //Debug.Log(locRot);

        if (!gotDear)
        {
            Quaternion sideRot = Quaternion.Euler(0, -camRotApp.x, 0);
            camHolderSide.transform.localRotation = Quaternion.Lerp(camHolderSide.transform.localRotation, sideRot, Time.deltaTime * 50);
        }
        else
        {
            Quaternion sideRot = Quaternion.LookRotation((-camHolderSide.transform.position + deerTarget.position), transform.up);
            camHolderSide.transform.rotation = Quaternion.Lerp(camHolderSide.transform.rotation, sideRot, Time.deltaTime * 5);
        }




        diff.y = Vector3.SignedAngle(transform.forward, camHolder.transform.forward, transform.up);

        //diff.x = Vector3.SignedAngle(transform.forward, camHolderSide.transform.forward, transform.up);

        //ClampAngle = camRotApp.x + diff.x;

        ClampAngle = 0;





        if (Input.GetMouseButtonDown(0) && !Input.GetMouseButton(1) && !Input.GetMouseButton(2) && !Input.GetMouseButton(3))
        {
            //camRotNew.x = -diffStored.y;
            camRotOld = camRotNew;
            clickInY = trackPos.y;
            clickInX = trackPos.x;
        }
        //if (Input.GetMouseButtonUp(0))
        //{
        //
        //}
    }

    void FollowPathFollower()
    {
        transform.position = Vector3.Lerp(transform.position, pathFollowing.instance.transform.position , Time.deltaTime * 5);
        transform.rotation = Quaternion.Lerp(transform.rotation, pathFollowing.instance.transform.rotation, Time.deltaTime * 2);
    }

    public void AllignSideCamera()
    {
        camHolder.transform.localRotation = Quaternion.Euler(camRotApp.y, 0, 0);
        camHolderSide.transform.rotation = transform.rotation;

        Vector3 sideRot = Mathf.Rad2Deg * Quaternion.ToEulerAngles(camHolderSide.transform.rotation);
        //Debug.Log(sideRot);

        camRotApp.x = camRotNew.x = camRotOld.x = -sideRot.y;

        diff.y = Vector3.SignedAngle(transform.forward, camHolder.transform.forward, transform.up);

        diff.x = Vector3.SignedAngle(transform.forward, camHolderSide.transform.forward, transform.up);

        ClampAngle = camRotApp.x + diff.x;
    }

    void ManageDummy(float x, float y)
    {
        //Dummy.transform.localRotation = Quaternion.Euler(y, x, 0);
        Vector3 thisRot =Mathf.Rad2Deg* Quaternion.ToEulerAngles(this.transform.rotation);
        //float camrot = 0;
        if (Input.GetMouseButtonDown(0) && !Input.GetMouseButton(1) && !Input.GetMouseButton(2) && !Input.GetMouseButton(3))
        {
            camrot = thisRot.y;
        }
        if (clicked)
        {
            camrotCorr = -thisRot.y + camrot;

            
        }

        //Quaternion LookRotation = Quaternion.Euler(y, x + camrotCorr, 0);

        Quaternion LookRotation = Quaternion.Euler(y, x , 0);

        camHolderSide.transform.localRotation = Quaternion.Lerp(camHolderSide.transform.localRotation, LookRotation, Time.deltaTime * 10f);
    }

    //public void GetDeer(Transform t)
    //{
    //    deerTarget = t;
    //    //gotDear = true;
    //}
    //
    //public void LeaveDeer()
    //{
    //    //gotDear = false;
    //}

    void RotateCamNew()
    {
        Quaternion LookRotation = Quaternion.LookRotation(-camHolderSide.transform.position +DummyPointer.transform.position, transform.up);
        camHolderSide.transform.rotation = Quaternion.Lerp(camHolderSide.transform.rotation, LookRotation, Time.deltaTime * 10f);

        
    }



    
}
