﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManageMent : MonoBehaviour
{
    public static GameManageMent instance;
    public int levelIndex;

    public int itemsCollected;
    public int maxItems;
    public int stars;

    public bool GotDeer;
    public bool LevelEnd;

    public GameObject DeerMesh;
    public DeerItselfBehavior DeerMeshScript;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        DeerMeshScript = DeerMesh.GetComponent<DeerItselfBehavior>();
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetDeer(Transform t)
    {
        t.position += new Vector3(0, 5, 0);
        PlayerMotorInGame.instance. deerTarget = t ;
        GotDeer = true;
        UiManage.instance.CapturePanelAppear();
        DeerMeshScript.TriggerHeadRise();
    }

    public void LeaveDeer()
    {
        GotDeer = false;
        UiManage.instance.CloseAllPanels();
        ItemHubCentral.instance.SinkAllHubs();
        ItemHubCentral.instance.skippedDeer = true;
    }

    public void TakeSnapShot()
    {
        SnapTaker.instance.TakeSnap();
        LevelEnd = true;
        UiManage.instance.LevelEndPanelAppear();
    }

    public void DeerMeshAppear(Transform t)
    {
        DeerMesh.transform.position = t.position;
        DeerMesh.transform.rotation = t.rotation;
        DeerMeshScript.AppearDeerMesh(GameManageMent.instance.stars);
    }
}
