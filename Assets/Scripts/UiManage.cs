﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManage : MonoBehaviour
{
    public static UiManage instance;
    public GameObject LevelEndPanel;
    public GameObject DecisionTurnPanel;
    public GameObject CapturePanel;
    public GameObject InventoryPanel;
    public Text collectedItemsText;
    public Image whiteImage;
    public Color whiteColor;
    public Color whiteTransColor;
    public float GotItemsUi;
    public int maxItemsUi
    {
        get
        {
            return GameManageMent.instance.maxItems;
        }
    }
    public GameObject[] stars;

    public Text collectedText;
    public Text collectedTextDescription;

    public bool Collected;

    public bool atDecisionTurn;

    public pathPointBehavior DecisionPointScript;

    public float colfac;

    public GameObject ExploreMoreButton;
    public Text GetMoreClues;

    public string[] clueCollectionTexts;
    public string[] clueCollectionTextsDescription;

    public Text GameUiCluesValue;
    public Button InventoryButton;

    public bool AtUi;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        collectedText.gameObject. SetActive(false);
        UpadteUiTexts();
        AppearInventoryPanel();
        InventorySystem.instance.StartFuntionOfInventorySystem();
        CloseAllPanels();
        
        //LevelEndPanel.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        ItemCountVisual();
        //UpadteUiTexts();
        //CollecetdTextVisual();
    }

    public void LevelEndPanelAppear()
    {
        LevelEndPanel.SetActive(true);

    }

    public void CapturePanelAppear()
    {
        if(GameManageMent.instance.itemsCollected >= GameManageMent.instance.maxItems)
        {
            ExploreMoreButton.SetActive(false);
            GetMoreClues.gameObject.SetActive(false);
        }
        else
        {
            ExploreMoreButton.SetActive(true);
            GetMoreClues.gameObject.SetActive(true);
        }
        CapturePanel.SetActive(true);
        LevelEndPanel.SetActive(false);
    }

    public void CloseAllPanels()
    {
        CapturePanel.SetActive(false);
        LevelEndPanel.SetActive(false);
        InventoryPanel.SetActive(false);
        AtUi = false;
    }

    public void ManagePanelAppearances()
    {
        //if(pathFollowing.instance.LevelEnd)
        //{
        //    LevelEndPanelAppear();
        //}
        //else
        //{
        //    LevelEndPanel.SetActive(false);
        //}
    }

    public void ItemCountVisual()
    {
        if (GameManageMent.instance.LevelEnd && GotItemsUi <= GameManageMent.instance.itemsCollected)
        {
            GotItemsUi += Time.deltaTime * 5;
            collectedItemsText.text =(int) GotItemsUi + " / "+ maxItemsUi;
            float starRatio = GotItemsUi / maxItemsUi;
            if(starRatio > 0.6f)
            {
                stars[0].SetActive(true);
                if (starRatio > 0.8f)
                {
                    
                    stars[1].SetActive(true);
                    if (starRatio > 0.99f)
                    {

                        stars[2].SetActive(true);
                    }
                }
            }

            if (colfac <= 1)
            {
                colfac += Time.deltaTime;
            }

        }
        else
        {
            
        }
        
        whiteImage.color = Color.Lerp(whiteColor, whiteTransColor, colfac);
    }

    public void RestartGame()
    {
        Application.LoadLevel(0);
    }

    public void CollecetdTextVisual(int clueIndex)
    {
        if (Collected)
        {
            collectedText.gameObject.SetActive(true);
            collectedText.text = clueCollectionTexts[clueIndex];
            collectedTextDescription.text = clueCollectionTextsDescription[clueIndex];

            Collected = false;
            CancelInvoke("DisappearCollectedText");
            Invoke("DisappearCollectedText", 3f);
        }
    }

    void DisappearCollectedText()
    {
        collectedText.gameObject.SetActive(false);
        CluesFloatingOnUiSystem.instance.DisAppearClueOnUI();
    }

    public void AppearDecisionTurn()
    {
        DecisionTurnPanel.SetActive(true);
    }
    public void DisAppearDecisionTurn()
    {
        DecisionTurnPanel.SetActive(false);
    }

    public void AppearInventoryPanel()
    {
        if (InventoryPanel.active)
        {
            InventoryPanel.SetActive(false);
            AtUi = false;
        }
        else 
        {
            InventoryPanel.SetActive(true);
            AtUi = true;
        }
        
    }
    //public void DisappearInventoryPanel()
    //{
    //    InventoryPanel.SetActive(false);
    //}


    public void GoRightCopy()
    {
        //DecisionPointScript.GoRight();
        pathFollowing.instance.RightDecision();
        UiManage.instance.DisAppearDecisionTurn();
        pathFollowing.instance.LeaveDecisionPoint();
    }

    public void GoLeftCopy()
    {
        //DecisionPointScript.GoLeft();
        pathFollowing.instance.LeftDecision();
        UiManage.instance.DisAppearDecisionTurn();
        pathFollowing.instance.LeaveDecisionPoint();
    }

    public void UpadteUiTexts()
    {
        GameUiCluesValue.text = "" + GameManageMent.instance.itemsCollected + " / " + GameManageMent.instance.maxItems;
    }

}
