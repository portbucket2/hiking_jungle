﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HubAriseSensorManager : MonoBehaviour
{
    public ItemsHubManager[] RefHubFront;
    public ItemsHubManager[] RefHubBack;
    public int[] refHubIdFront;
    public int[] refHubIdBack;
    public bool automatic;
    public int refHubPointingId;
    public int RighthubId;
    public int LefthubId;

    public float Angle1;
    public float Angle2;

    // Start is called before the first frame update
    void Start()
    {
        AssignHubIds();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void DecideHubId(Transform t)
    {
        Angle1 = Vector3.Angle(t.forward, transform.forward);
        Angle2 = Vector3.Angle(t.forward, -transform.forward);
        int size = 0;
        
        if (Angle1 < Angle2)
        {
            size = RefHubFront.Length;
            if (size == 1)
            {
                //RefHubFront
                ItemHubCentral.instance.AriseHub(refHubIdFront[0], 0, 1);
            }
            if (size == 2)
            {
                ItemHubCentral.instance.AriseHub(refHubIdFront[0], refHubIdFront[1],2);
                
            }
        }
        else
        {
            size = RefHubBack.Length;
            if (size == 1)
            {
                ItemHubCentral.instance.AriseHub(refHubIdBack[0], 0, 1);
            }
            if (size == 2)
            {
                ItemHubCentral.instance.AriseHub(refHubIdBack[0], refHubIdBack[1], 2);
            }
        }

        //ItemHubCentral.instance.AriseHub(1);
    }

    public void AssignHubIds()
    {
        refHubIdFront = new int[RefHubFront.Length];
        refHubIdBack = new int[RefHubBack.Length];
        for (int i = 0; i < RefHubFront.Length; i++)
        {
            refHubIdFront[i] = RefHubFront[i].OwnHubId;
        }

        for (int i = 0; i < RefHubBack.Length; i++)
        {
            refHubIdBack[i] = RefHubBack[i].OwnHubId;
        }
    }

    
}
