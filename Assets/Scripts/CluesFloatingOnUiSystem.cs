﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CluesFloatingOnUiSystem : MonoBehaviour
{
    public Animator clueFloatAnim;
    public GameObject[] cluesMeshUi;
    public GameObject[] cluesMeshUiOutlined;
    public static CluesFloatingOnUiSystem instance;
    public ScriptableItemObj ScriptableItem;
    public Transform cluesParent;
    public Transform cluesOutlinedParent;

    public int AnimUpdateTrack;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        AssignItemsFirst();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AppearClueOnUI(int ClueIndex)
    {
        for (int i = 0; i < cluesMeshUi.Length; i++)
        {
            if(i == ClueIndex)
            {
                cluesMeshUi[i].SetActive(true);
                cluesMeshUiOutlined[i].SetActive(true);
                //Debug.Log("clue " + i);

            }
            else
            {
                cluesMeshUi[i].SetActive(false);
                cluesMeshUiOutlined[i].SetActive(false);
            }
        }

        if(AnimUpdateTrack == 0)
        {
            clueFloatAnim.SetTrigger("ClueAppearUi");
        }
        
        AnimUpdateTrack = 1;
    }

    public void DisAppearClueOnUI()
    {

        if (AnimUpdateTrack == 1)
        {
            clueFloatAnim.SetTrigger("ClueDisAppearUi");
        }
            
        AnimUpdateTrack = 0;
    }

    public void AssignItemsFirst()
    {
        for (int i = 0; i < ScriptableItem.cluesLvlOne.Length; i++)
        {
            GameObject go = Instantiate(ScriptableItem.cluesLvlOne[i], cluesParent.transform.position, cluesParent.transform.rotation);
            go.transform.SetParent(cluesParent.transform);
            go.transform.localScale = new Vector3(1, 1, 1);
            cluesMeshUi[i] = go;
            go.SetActive(false);

            go = Instantiate(ScriptableItem.cluesLvlOneOutline[i], cluesOutlinedParent.transform.position, cluesOutlinedParent.transform.rotation);
            go.transform.SetParent(cluesOutlinedParent.transform);
            go.transform.localScale = new Vector3(1, 1, 1);
            cluesMeshUiOutlined[i] = go;
            go.SetActive(false);
        }
        
    }
}
