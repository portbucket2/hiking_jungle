﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BushPrefabMaker : MonoBehaviour
{
    public Vector3 size;
    public float minScale;
    public float maxScale;
    public int LeavesCount;
    public GameObject bushLeafUnit;
    public GameObject[] leaves;

    public Quaternion lookRot;

    public GameObject sizeBox;

    public GameObject editorGarbage;

    // Start is called before the first frame update
    void Start()
    {
        //size = sizeBox.transform.localScale;

        sizeBox.SetActive(false);
        editorGarbage.SetActive(false);

        leaves = new GameObject[LeavesCount];

        for (int i = 0; i < leaves.Length; i++)
        {
            float x = Random.Range(0, size.x);
            float y = Random.Range(0, size.y);
            float z = Random.Range(0, size.z);

            float s = Random.Range(minScale, maxScale);

            GameObject GO = Instantiate(bushLeafUnit , transform.position- new Vector3(size.x/2, 0, size.z/2) + new Vector3( x,y,z), Quaternion.identity);
            GO.transform.localScale = new Vector3(s,s,s);
            leaves[i] = GO;
            leaves[i].transform.SetParent(transform);
        }
    }

    // Update is called once per frame
    void Update()
    {
        CameraFacing();
    }

    void CameraFacing()
    {
        lookRot = Quaternion.LookRotation(-Camera.main.transform.position + transform.position, Vector3.up);

        //lookRot =transform.LookAt()


        for (int i = 0; i < leaves.Length; i++)
        {
            leaves[i].transform.rotation = lookRot;
        }
    }


    public void EditorStart()
    {
        leaves = new GameObject[0];
        foreach (Transform child in editorGarbage.transform)
        {
            GameObject.DestroyImmediate(child.gameObject);
        }
        //editorGarbage.transform.GetChild;
        sizeBox.SetActive(false);

        leaves = new GameObject[LeavesCount];
        GameObject gr = new GameObject();
        gr.transform.SetParent(editorGarbage.transform);
        for (int i = 0; i < leaves.Length; i++)
        {
            float x = Random.Range(0, size.x);
            float y = Random.Range(0, size.y);
            float z = Random.Range(0, size.z);
        
            float s = Random.Range(minScale, maxScale);
        
            GameObject GO = Instantiate(bushLeafUnit, transform.position - new Vector3(size.x / 2, 0, size.z / 2) + new Vector3(x, y, z), Quaternion.identity);
            GO.transform.localScale = new Vector3(s, s, s);
            leaves[i] = GO;
            leaves[i].transform.SetParent(gr. transform);
        }
    }
}
